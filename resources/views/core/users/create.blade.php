@extends('layouts.master')
@section('title', $title)
@section('content')
    <div class="container my-5">
        <div class="row">
            <div class="offset-3 col-md-6">
                {{ Form::open(['route'=>'admin.users.store', 'id' => 'userCreateForm']) }}
                @include('core.users._create_form')
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('plugins/cvalidator/cvalidator-language-en.js') }}"></script>
    <script src="{{ asset('plugins/cvalidator/cvalidator.js') }}"></script>
    <script>
        $(document).ready(function () {
            var cForm = $('#userCreateForm').cValidate({
                rules : {
                    'first_name' : 'required|escapeInput|alphaSpace',
                    'last_name' : 'required|escapeInput|alphaSpace',
                    'email' : 'required|escapeInput|email',
                    'username' : 'required|escapeInput',
                    'address' : 'escapeInput',
                    'assigned_role' : 'required|in:{{ array_to_string($roles) }}',
                    'is_email_verified' : 'required|in:{{ array_to_string(email_status()) }}',
                    'is_active' : 'required|in:{{ array_to_string(account_status()) }}',
                    'is_financial_active' : 'required|in:{{ array_to_string(financial_status()) }}',
                    'is_accessible_under_maintenance' : 'required|in:{{ array_to_string(maintenance_accessible_status()) }}',
                }
            });
        });
    </script>
@endsection
