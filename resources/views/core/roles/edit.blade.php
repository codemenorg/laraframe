@extends('layouts.master')
@section('title', $title)
@section('content')
    <div class="container-fluid my-5">
        {{ Form::model($role, ['route'=>['roles.update',[$role->slug, $type]], 'method'=>'PUT','id' => 'roleForm']) }}
        @include('core.roles._form',['buttonText'=>__('Update')])
        {{ Form::close() }}
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/role_manager.js') }}"></script>
    <script src="{{ asset('plugins/cvalidator/cvalidator-language-en.js') }}"></script>
    <script src="{{ asset('plugins/cvalidator/cvalidator.js') }}"></script>
    <script>
        $(document).ready(function () {
            var cForm = $('#roleForm').cValidate({
                rules : {
                    'name' : 'required|escapeInput'
                }
            });
        });
    </script>
@endsection
