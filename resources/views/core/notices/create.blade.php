@extends('layouts.master')
@section('title', $title)
@section('content')
    <div class="container my-5">
        <div class="row">
            <div class="offset-md-3 col-md-6">
                {{ Form::open(['route'=>'notices.store', 'method' => 'post', 'class'=>'form-horizontal', 'id' => 'noticeForm']) }}
                @include('core.notices._form',['buttonText'=> __('Create')])
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('plugins/bs4-datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('script')
    @include('core.notices._script')
@endsection
