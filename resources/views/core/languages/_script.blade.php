<script src="{{ asset('plugins/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('plugins/cvalidator/cvalidator-language-en.js') }}"></script>
<script src="{{ asset('plugins/cvalidator/cvalidator.js') }}"></script>
<script>
    $(document).ready(function () {
        var cForm =$('#languageForm').cValidate({
            rules : {
                'name' : 'required|escapeInput',
                'short_code' : 'required|escapeInput|min:2|max:2',
                'icon' : 'image|max:100',
                'is_active' : 'required|escapeInput|in:{{ array_to_string(active_status()) }}'
            }
        });
    });
</script>
