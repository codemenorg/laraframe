@extends('layouts.master',['headerLess'=>true])
@section('title', __('Your are in a demo mode'))
@section('content')
    @component('components.auth')
        <h2 class="text-center text-warning mb-4">{{ __('Your are in a demo mode')  }}</h2>
        <p class="text-center">{{ __("Modification is not available in demo mode.")}}</p>
    @endcomponent
@endsection
