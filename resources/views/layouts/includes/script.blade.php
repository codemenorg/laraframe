</div>
@if(!isset($headerLess) || !$headerLess)
    @if($isSideNavActive)
        @include('layouts.includes.side_nav')
    @endif
@endif
@include('layouts.includes.notice')
<!-- Flash Message -->
@include('errors.flash_message')

<!-- REQUIRED SCRIPTS -->
@yield('script-top')
<!-- jQuery -->
<script src="{{ asset('js/app.js') }}"></script>
@if(!isset($headerLess) || !$headerLess)
    <script src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('plugins/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('plugins/slicknav/slicknav.min.js') }}"></script>
@endif
@if(!isset($headerLess) || !$headerLess)
    <link rel="stylesheet" href="{{ asset('plugins/slicknav/slicknav.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.css') }}">
@endif
@if(isset($directionMode))
    @if($directionMode==PAGE_DIRECTION_RIGHT_TO_LEFT)
        <script src="{{ asset('plugins/bootstrap/bootstrap-rtl.min.js') }}"></script>
    @else
        <script src="{{ asset('plugins/bootstrap/bootstrap.min.js') }}"></script>
    @endif
@else
    @if(language(App()->getLocale())['direction']==PAGE_DIRECTION_RIGHT_TO_LEFT)
        <script src="{{ asset('plugins/bootstrap/bootstrap-rtl.min.js') }}"></script>
    @else
        <script src="{{ asset('plugins/bootstrap/bootstrap.min.js') }}"></script>
    @endif
@endif
@yield('extra-script')
<script src="{{ asset('plugins/flash_message/flash.message.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>

@yield('script')

</body>
</html>
