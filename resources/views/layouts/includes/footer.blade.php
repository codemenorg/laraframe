<footer class="footer bg-primary ">
    <div class="container">
        <div class="row">
            <div class="col-12 py-3 text-white text-center">
                {{__('All Rights Reserved | Powered by CodeMen')}}
            </div>
        </div>
    </div>
</footer>
