@extends('layouts.master')
@section('title', $title)
@section('content')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-md-7">
                {{ Form::open(['route'=>'tickets.store', 'files'=> true, 'id' => 'ticketForm']) }}
                @include('user.tickets._form',['buttonText' => __('Create')])
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('plugins/cvalidator/cvalidator-language-en.js') }}"></script>
    <script src="{{ asset('plugins/cvalidator/cvalidator.js') }}"></script>
    <script>
        $(document).ready(function () {
            var cForm = $('#ticketForm').cValidate({
                rules: {
                    'title': 'required|escapeInput',
                    'content': 'required|escapeText',
                    'previous_id': 'escapeInput',
                    'attachment': 'mimetypes:jpg,png,jpeg,doc,pdf,docx,txt|max:1024',
                }
            });
        });
    </script>
@endsection

