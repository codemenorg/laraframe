<?php

use App\Http\Controllers\Web\Core\{NotificationController, ProfileController};
use App\Http\Controllers\Web\Guest\AuthController;
use App\Http\Controllers\Web\Ticket\UserTicketController;
use Illuminate\Support\Facades\Route;

Route::get('logout', [AuthController::class, 'logout'])->name('logout');

//User profile
Route::get('profile', [ProfileController::class, 'index'])->name('profile.index');
Route::get('profile/edit', [ProfileController::class, 'edit'])->name('profile.edit');
Route::put('profile/update', [ProfileController::class, 'update'])->name('profile.update');
Route::get('profile/change-password', [ProfileController::class, 'changePassword'])->name('profile.change-password');
Route::put('profile/change-password/update', [ProfileController::class, 'updatePassword'])->name('profile.update-password');
Route::get('profile/avatar/edit', [ProfileController::class, 'avatarEdit'])->name('profile.avatar.edit');
Route::put('profile/avatar/update', [ProfileController::class, 'avatarUpdate'])->name('profile.avatar.update');

//User Specific Notice
Route::get('notifications', [NotificationController::class, 'index'])->name('notifications.index');
Route::get('notifications/{notification}/read', [NotificationController::class, 'markAsRead'])->name('notifications.mark-as-read');
Route::get('notifications/{notification}/unread', [NotificationController::class, 'markAsUnread'])->name('notifications.mark-as-unread');

//Ticket Management
Route::resource('tickets', UserTicketController::class)->only(['index', 'create', 'store', 'show']);
Route::put('tickets/{ticket}/close', [UserTicketController::class, 'close'])->name('tickets.close');
Route::post('tickets/{ticket}/comment', [UserTicketController::class, 'comment'])->name('tickets.comment.store');
Route::get('ticket/{ticket}/download-attachment/{fileName}', [UserTicketController::class, 'download'])->name('tickets.attachment.download');

Route::get('/usertest', function () {
    return "Tested";
})->name('user.test');
