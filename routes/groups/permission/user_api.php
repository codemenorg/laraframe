<?php

use App\Http\Controllers\Api\Ticket\TicketController;
use Illuminate\Support\Facades\Route;

Route::name('api.')->group(function () {
    Route::get('tickets', [TicketController::class, 'index'])->name('tickets.index');
});
