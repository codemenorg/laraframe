<?php

use App\Http\Controllers\Web\Core\{AdminDirectoryController,
    AdminMediaController,
    AdminUserSearchController,
    ApplicationSettingController,
    LanguageController,
    NavigationController,
    NoticesController,
    RoleController,
    UsersController};
use App\Http\Controllers\Web\Page\AdminDynamicContentController;
use App\Http\Controllers\Web\Page\AdminPageController;
use App\Http\Controllers\Web\Page\AdminVisualEditController;
use App\Http\Controllers\Web\Ticket\AdminTicketController;
use Illuminate\Support\Facades\Route;
use Rap2hpoutre\LaravelLogViewer\LogViewerController;

Route::group(['prefix' => 'admin'], function () {
    //User Group Role
    Route::resource('roles', RoleController::class)
        ->except(['show', 'edit', 'update']);
    Route::put('roles/{role}/change-status', [RoleController::class, 'changeStatus'])->name('roles.status');
    Route::get('roles/{role}/{type}/permissions', [RoleController::class, 'edit'])->name('roles.edit');
    Route::put('roles/{role}/{type}/permissions', [RoleController::class, 'update'])->name('roles.update');

    //Application Setting
    Route::get('application-settings', [ApplicationSettingController::class, 'index'])
        ->name('application-settings.index');
    Route::get('application-settings/{type}/{sub_type}', [ApplicationSettingController::class, 'edit'])
        ->name('application-settings.edit');
    Route::put('application-settings/{type}/update/{sub_type?}', [ApplicationSettingController::class, 'update'])
        ->name('application-settings.update');

    //Admin Notice
    Route::resource('notices', NoticesController::class)->except(['show']);

    Route::get('menu-manager/{menu_slug?}', [NavigationController::class, 'index'])->name('menu-manager.index');
    Route::post('menu-manager/{menu_slug?}/save', [NavigationController::class, 'save'])->name('menu-manager.save');

    //Language
    Route::get('languages/settings', [LanguageController::class, 'settings'])->name('languages.settings');
    Route::get('languages/translations', [LanguageController::class, 'getTranslation'])->name('languages.translations');
    Route::put('languages/settings', [LanguageController::class, 'settingsUpdate'])->name('languages.update.settings');
    Route::put('languages/sync', [LanguageController::class, 'sync'])->name('languages.sync');
    Route::resource('languages', LanguageController::class)->except('show');

    //Ticket Management
    Route::put('tickets/{ticket}/close', [AdminTicketController::class, 'close'])->name('admin.tickets.close');
    Route::put('tickets/{ticket}/resolve', [AdminTicketController::class, 'resolve'])->name('admin.tickets.resolve');
    Route::put('tickets/{ticket}/assign', [AdminTicketController::class, 'assign'])->name('admin.tickets.assign');
    Route::post('tickets/{ticket}/comment', [AdminTicketController::class, 'comment'])->name('admin.tickets.comment.store');
    Route::get('ticket/{ticket}/download-attachment/{fileName}', [AdminTicketController::class, 'download'])->name('admin.tickets.attachment.download');
    Route::resource('tickets', AdminTicketController::class)->only(['index', 'show'])->names('admin.tickets');

    //User Managements
    Route::get('users/{user}/edit/status', [UsersController::class, 'editStatus'])->name('admin.users.edit.status');
    Route::put('users/{user}/update/status', [UsersController::class, 'updateStatus'])->name('admin.users.update.status');
    Route::get('users/search', AdminUserSearchController::class)->name('admin.users.search');
    Route::resource('users', UsersController::class)->names('admin.users');

    //Laravel Log Viewer
    Route::get('logs', [LogViewerController::class, 'index'])->name('logs.index');

    //Page Management
    Route::resource('pages', AdminPageController::class)->names('admin.pages')->except('show');
    Route::get('pages/{page}/visual-edit', [AdminVisualEditController::class, 'edit'])->name('admin.pages.visual-edit');
    Route::put('pages/{page}/visual-update', [AdminVisualEditController::class, 'update'])->name('admin.pages.visual-update');
    Route::get('pages/{page}/visual-edit/{editable_lang}', [AdminVisualEditController::class, 'editOtherLang'])->name('admin.pages.visual-lang-edit');
    Route::put('pages/{page}/visual-update/{editable_lang}', [AdminVisualEditController::class, 'updateOtherLang'])->name('admin.pages.visual-lang-update');
    Route::put('pages/{page}/published', [AdminPageController::class, 'togglePublish'])->name('admin.pages.published');
    Route::put('pages/{page}/home-page', [AdminPageController::class, 'makeHomePage'])->name('admin.pages.home-page');

    Route::get('get-dynamic-content', AdminDynamicContentController::class)->name('admin.dynamic-content');

    // Media manager
    Route::resource('media', AdminMediaController::class)->names('admin.media')->only(['index','store','destroy']);

    Route::post('media/directories', [AdminDirectoryController::class,'store'])->name('admin.directories.store');
    Route::put('media/directories/update', [AdminDirectoryController::class,'update'])->name('admin.directories.update');
    Route::delete('media/directories/delete', [AdminDirectoryController::class,'delete'])->name('admin.directories.delete');
});




