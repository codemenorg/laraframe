<?php

use App\Http\Controllers\Api\User\AdminUserController;
use Illuminate\Support\Facades\Route;

Route::prefix('admin')->name('api.')->group(function () {
    Route::get('users', [AdminUserController::class, 'index'])->name('admin.users.index');
});

