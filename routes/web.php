<?php

use App\Http\Controllers\Web\HomeController;
use App\Http\Controllers\Web\Page\PageController;
use App\Http\Controllers\Web\TestController;
use Illuminate\Support\Facades\Route;

//Test
Route::get('test', [TestController::class, 'test'])->name('test');

Route::get('/', HomeController::class)->name('home');

Route::get('/{page:slug}', PageController::class)->name('page');
