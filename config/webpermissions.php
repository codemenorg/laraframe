<?php

return [
    'configurable_routes' => [
        'admin_section' => [
            'application-settings' => [
                ROUTE_GROUP_READER_ACCESS => [
                    'application-settings.index',
                ],
                ROUTE_GROUP_MODIFIER_ACCESS => [
                    'application-settings.edit',
                    'application-settings.update',
                ],
            ],
            'role_managements' => [
                ROUTE_GROUP_READER_ACCESS => [
                    'roles.index',
                ],
                ROUTE_GROUP_CREATION_ACCESS => [
                    'roles.create',
                    'roles.store',
                ],
                ROUTE_GROUP_MODIFIER_ACCESS => [
                    'roles.edit',
                    'roles.update',
                    'roles.status',
                ],
                ROUTE_GROUP_DELETION_ACCESS => [
                    'roles.destroy',
                ],
            ],
            'user_managements' => [
                ROUTE_GROUP_READER_ACCESS => [
                    'users.index',
                    'users.show',
                ],
                ROUTE_GROUP_CREATION_ACCESS => [
                    'users.create',
                    'users.store',
                ],
                ROUTE_GROUP_MODIFIER_ACCESS => [
                    'users.edit',
                    'users.update',
                ],
                ROUTE_GROUP_DELETION_ACCESS => [
                    'users.update.status',
                    'users.edit.status',
                ],
            ],
            'notice_managements' => [
                ROUTE_GROUP_READER_ACCESS => [
                    'notices.index',
                    'notices.show'
                ],
                ROUTE_GROUP_CREATION_ACCESS => [
                    'notices.create',
                    'notices.store'
                ],
                ROUTE_GROUP_MODIFIER_ACCESS => [
                    'notices.edit',
                    'notices.update',
                ],
                ROUTE_GROUP_DELETION_ACCESS => [
                    'notices.destroy',
                ]
            ],
            'menu_manager' => [
                ROUTE_GROUP_FULL_ACCESS => [
                    'menu-manager.index',
                    'menu-manager.save',
                ],
            ],
            'log_viewer' => [
                ROUTE_GROUP_READER_ACCESS => [
                    'logs.index'
                ]
            ],
            'language_managements' => [
                ROUTE_GROUP_READER_ACCESS => [
                    'languages.index',
                    'languages.settings',
                    'languages.translations'
                ],
                ROUTE_GROUP_CREATION_ACCESS => [
                    'languages.create',
                    'languages.store'
                ],
                ROUTE_GROUP_MODIFIER_ACCESS => [
                    'languages.edit',
                    'languages.update',
                    'languages.update.settings',
                    'languages.sync',
                ],
                ROUTE_GROUP_DELETION_ACCESS => [
                    'languages.destroy'
                ]
            ],
            'page_managements'=>[
                ROUTE_GROUP_READER_ACCESS => [
                    'admin.pages.index',
                    'admin.dynamic-content',
                ],
                ROUTE_GROUP_CREATION_ACCESS => [
                    'admin.pages.create',
                    'admin.pages.store'
                ],
                ROUTE_GROUP_MODIFIER_ACCESS => [
                    'admin.pages.edit',
                    'admin.pages.update',
                    'admin.pages.visual-edit',
                    'admin.pages.visual-update',
                    'admin.pages.published',
                    'admin.pages.home-page',
                    'admin.pages.visual-lang-edit',
                    'admin.pages.visual-lang-update',
                ],
                ROUTE_GROUP_DELETION_ACCESS => [
                    'admin.pages.destroy',
                ]
            ],
            'media_manager'=>[
                ROUTE_GROUP_READER_ACCESS => [
                    'admin.media.index',
                ],
                ROUTE_GROUP_CREATION_ACCESS => [
                    'admin.media.store',
                    'admin.directories.store'
                ],
                ROUTE_GROUP_MODIFIER_ACCESS => [
                    'admin.directories.update'
                ],
                ROUTE_GROUP_DELETION_ACCESS => [
                    'admin.media.destroy',
                    'admin.directories.delete',
                ]
            ],
            'tickets' => [
                ROUTE_GROUP_READER_ACCESS => [
                    'admin.tickets.index',
                    'admin.tickets.show',
                    'admin.tickets.attachment.download'
                ],
                ROUTE_GROUP_MODIFIER_ACCESS => [
                    'admin.tickets.close',
                    'admin.tickets.resolve',
                    'admin.tickets.assign',
                ],
                'commenting_access' => [
                    'admin.tickets.comment.store',
                ]
            ]
        ],
        'user_section' => [
            'tickets' => [
                ROUTE_GROUP_READER_ACCESS => [
                    'tickets.index',
                    'tickets.show',
                    'tickets.attachment.download'
                ],
                ROUTE_GROUP_CREATION_ACCESS => [
                    'tickets.create',
                    'tickets.store',
                ],
                'closing_access' => [
                    'tickets.close',
                ],
                'commenting_access' => [
                    'tickets.comment.store',
                ]
            ]
        ]
    ],
    ROUTE_TYPE_ROLE_BASED => [
        USER_ROLE_ADMIN => [

        ],
        USER_ROLE_USER => [

        ]
    ],

    ROUTE_TYPE_AVOIDABLE_MAINTENANCE => [
        'login',
        'tickets.index',
    ],

    ROUTE_TYPE_AVOIDABLE_UNVERIFIED => [
        'logout',
        'profile.index',
        'notifications.index',
        'notifications.mark-as-read'
    ],
    ROUTE_TYPE_AVOIDABLE_INACTIVE => [
        'logout',
        'profile.index',
        'notifications.index',
        'notifications.mark-as-read',
        'notifications.mark-as-unread',
    ],
    ROUTE_TYPE_FINANCIAL => [

    ],

    ROUTE_TYPE_GLOBAL => [
        'logout',
        'profile.index',
        'profile.edit',
        'profile.update',
        'profile.change-password',
        'profile.update-password',
        'profile.avatar.edit',
        'profile.avatar.update',
        'notifications.index',
        'notifications.mark-as-read',
        'notifications.mark-as-unread',
    ],

];
