<?php
return [

    'fixed_roles' => [USER_ROLE_ADMIN, USER_ROLE_USER],

    'path_profile_image' => 'images/users/',
    'path_image' => 'images/',
    'language_icon' => 'images/languages/',
    'ticket_attachment' => 'images/tickets/',
    'email_status' => [
        ACTIVE => ['color_class' => 'success'],
        INACTIVE => ['color_class' => 'danger'],
    ],
    'active_status' => [
        ACTIVE => ['color_class' => 'success'],
        INACTIVE => ['color_class' => 'danger'],
    ],
    'account_status' => [
        STATUS_ACTIVE => ['color_class' => 'success'],
        STATUS_INACTIVE => ['color_class' => 'warning'],
        STATUS_DELETED => ['color_class' => 'danger'],
    ],
    'financial_status' => [
        ACTIVE => ['color_class' => 'success'],
        INACTIVE => ['color_class' => 'danger'],
    ],
    'maintenance_accessible_status' => [
        ACTIVE => ['color_class' => 'success'],
        INACTIVE => ['color_class' => 'danger'],
    ],

    'ticket_status' => [
        STATUS_OPEN => ['color_class' => 'info'],
        STATUS_IN_PROGRESS => ['color_class' => 'warning'],
        STATUS_RESOLVED => ['color_class' => 'success'],
        STATUS_CLOSED => ['color_class' => 'danger'],
    ],

    'image_extensions' => ['png', 'jpg', 'jpeg', 'gif'],

    'strip_tags' => [
        'escape_text' => ['beginning_text', 'ending_text', 'body', 'currentLangData'],
    ],

    'available_commands' => [
        'cache' => 'cache:clear',
        'config' => 'config:clear',
        'route' => 'route:clear',
        'view' => 'view:clear',
    ],

];
