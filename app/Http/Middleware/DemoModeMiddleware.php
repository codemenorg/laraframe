<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

class DemoModeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('DEMO_MODE', false) && in_array(strtoupper($request->method()), ['PUT', 'PATCH', 'DELETE'])) {
            if ($request->ajax() || $request->isJson() || $request->is('api/*')) {
                return response()->json([
                    RESPONSE_STATUS_KEY => false,
                    RESPONSE_MESSAGE_KEY => __('Modification is not available in demo mode.')
                ], 401);
            }
            throw new UnauthorizedException(ROUTE_REDIRECT_TO_DEMO_MODE);
        }
        return $next($request);
    }
}
