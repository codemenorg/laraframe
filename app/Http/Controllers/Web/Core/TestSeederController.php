<?php

namespace App\Http\Controllers\Web\Core;


use App\Http\Controllers\Controller;
use Codemen\Installer\Services\DatabaseService;
use Illuminate\Http\{RedirectResponse, Request};
use Illuminate\View\View;

class TestSeederController extends Controller
{

    private $databaseService;

    public function __construct(DatabaseService $databaseService)
    {

        $this->databaseService = $databaseService;
    }

    public function view($type, $routeConfig): View
    {
        $title = __('Test Data Seeding');
        return view('vendor.installer.seeding',
            compact(
                'type',
                'routeConfig',
                'title'
            )
        );
    }

    public function store(Request $request): RedirectResponse
    {
        $type = $request->route('types');
        set_time_limit(120);
        $response = $this->databaseService->testSeed();
        return redirect()->route('installer.types', $type)->with(compact('response'));
    }

}

