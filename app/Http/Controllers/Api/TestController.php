<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function test()
    {
        return response()->json([
            'data' => [
                'Laraframe' => 'laraframe.com',
                'Trademen' => 'trademen.codemen.org',
                'Cryptomania' => 'cryptomania.codemen.org',
                'Cryptofast' => 'cryptofast.codemen.org',
                'Tokener' => 'tokener.codemen.org',
                'Auctioneer' => 'auctioneer.codemen.org',
                '360 Design' => '360design.codemen.org',
                'Ganwala' => 'ganwala.codemen.org',
                'Pathshala' => 'pathshala.codemen.org',
            ]
        ]);
    }
}
