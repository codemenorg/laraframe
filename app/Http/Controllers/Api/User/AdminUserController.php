<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\Core\User;

class AdminUserController extends Controller
{
    public function index()
    {
        return User::with(["profile:user_id,first_name,last_name"])
            ->orderByDesc('created_at')
            ->paginate(PAGINATION_ITEM_PER_PAGE);
    }
}
