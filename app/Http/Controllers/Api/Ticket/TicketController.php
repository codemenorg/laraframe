<?php

namespace App\Http\Controllers\Api\Ticket;

use App\Http\Controllers\Controller;
use App\Models\Core\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function index()
    {
        return Ticket::where('user_id', auth()->id())
            ->orderByDesc('created_at')
            ->paginate(PAGINATION_ITEM_PER_PAGE);
    }
}
