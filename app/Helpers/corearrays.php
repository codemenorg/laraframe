<?php

if (!function_exists('no_header_layout')) {
    function no_header_layout($input = null)
    {
        $output = [
            0 => __('Dark'),
            1 => __('Light'),
        ];
        return is_null($input) ? $output : $output[$input];
    }
}
if (!function_exists('top_nav_type')) {
    function top_nav_type($input = null)
    {
        $output = [
            0 => __('Dark'),
            1 => __('Light'),
        ];
        return is_null($input) ? $output : $output[$input];
    }
}
if (!function_exists('side_nav_type')) {
    function side_nav_type($input = null)
    {
        $output = [
            0 => __('Dark'),
            1 => __('Light'),
            2 => __('Dark Transparent'),
            3 => __('light Transparent'),
        ];
        return is_null($input) ? $output : $output[$input];
    }
}
if (!function_exists('navigation_type')) {
    function navigation_type($input = null)
    {
        $output = [
            0 => __('Top navigation'),
            1 => __('Side navigation'),
            2 => __('Both'),
        ];
        return is_null($input) ? $output : $output[$input];
    }
}
if (!function_exists('inversed_logo')) {
    function inversed_logo($input = null)
    {
        $output = [
            ACTIVE => __('Enabled'),
            INACTIVE => __('Disabled')
        ];
        return is_null($input) ? $output : $output[$input];
    }
}
if (!function_exists('maintenance_status')) {
    function maintenance_status($input = null)
    {
        $output = [
            ACTIVE => __('Enabled'),
            INACTIVE => __('Disabled')
        ];
        return is_null($input) ? $output : $output[$input];
    }
}

if (!function_exists('email_status')) {
    function email_status($input = null)
    {
        $output = [
            ACTIVE => __('Verified'),
            INACTIVE => __('Unverified')
        ];

        return is_null($input) ? $output : $output[$input];
    }
}

if (!function_exists('financial_status')) {
    function financial_status($input = null)
    {
        $output = [
            ACTIVE => __('Active'),
            INACTIVE => __('Inactive')
        ];

        return is_null($input) ? $output : $output[$input];
    }
}

if (!function_exists('maintenance_accessible_status')) {
    function maintenance_accessible_status($input = null)
    {
        $output = [
            ACTIVE => __('Enable'),
            INACTIVE => __('Disable')
        ];

        return is_null($input) ? $output : $output[$input];
    }
}

if (!function_exists('account_status')) {
    function account_status($input = null)
    {
        $output = [
            STATUS_ACTIVE => __('Active'),
            STATUS_INACTIVE => __('Suspended'),
            STATUS_DELETED => __('Deleted')
        ];

        return is_null($input) ? $output : $output[$input];
    }
}

if (!function_exists('active_status')) {
    function active_status($input = null)
    {
        $output = [
            ACTIVE => __('Active'),
            INACTIVE => __('Inactive'),
        ];

        return is_null($input) ? $output : $output[$input];
    }
}

if (!function_exists('api_permission')) {
    function api_permission($input = null)
    {
        $output = [
            ROUTE_REDIRECT_TO_UNAUTHORIZED => '401',
            ROUTE_REDIRECT_TO_UNDER_MAINTENANCE => 'under_maintenance',
            ROUTE_REDIRECT_TO_EMAIL_UNVERIFIED => 'email_unverified',
            ROUTE_REDIRECT_TO_ACCOUNT_SUSPENDED => 'account_suspension',
            ROUTE_REDIRECT_TO_FINANCIAL_ACCOUNT_SUSPENDED => 'financial_suspension',
            REDIRECT_ROUTE_TO_USER_AFTER_LOGIN => 'login_success',
            REDIRECT_ROUTE_TO_LOGIN => 'login',
        ];

        return is_null($input) ? $output : $output[$input];
    }
}

if (!function_exists('language_switcher_items')) {
    function language_switcher_items($input = null)
    {
        $output = [
            'name' => __('Name'),
            'short_code' => __('Short Code'),
            'icon' => __('Icon')
        ];
        return is_null($input) ? $output : $output[$input];
    }
}

if (!function_exists('notices_types')) {
    function notices_types($input = null)
    {
        $output = [
            'warning' => __('Warning'),
            'danger' => __('Critical'),
            'info' => __('Info')
        ];
        return is_null($input) ? $output : $output[$input];
    }
}
if (!function_exists('notices_visible_types')) {
    function notices_visible_types($input = null)
    {
        $output = [
            VISIBLE_TYPE_PUBLIC => __('Public'),
            VISIBLE_TYPE_PRIVATE => __('Logged In User Only'),
        ];
        return is_null($input) ? $output : $output[$input];
    }
}

if (!function_exists('ticket_status')) {
    function ticket_status($input = null)
    {
        $output = [
            STATUS_OPEN => __('Open'),
            STATUS_IN_PROGRESS => __('In Progress'),
            STATUS_RESOLVED => __('Resolved'),
            STATUS_CLOSED => __('Closed'),
        ];

        return is_null($input) ? $output : $output[$input];
    }
}


if (!function_exists('datatable_downloadable_type')) {
    function datatable_downloadable_type($input = null)
    {
        $output = [
            'dompdf' => [
                'extension' => 'pdf',
                'label' => __('Download as PDF'),
                'icon_class' => 'fa fa-file-pdf-o text-danger'
            ],
            'csv' => [
                'extension' => 'csv',
                'label' => __('Download as CSV'),
                'icon_class' => 'fa fa-file-excel-o text-success'
            ]
        ];
        return is_null($input) ? $output : $output[$input];
    }
}

if (!function_exists('menu_types')) {
    function menu_types($input = null)
    {
        $output = [
            MENU_TYPE_ROUTE => __('Route'),
            MENU_TYPE_LINK => __('Link'),
            MENU_TYPE_PAGE => __('Page'),
        ];

        return is_null($input) ? $output : $output[$input];
    }
}


