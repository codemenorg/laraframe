<?php

const LANGUAGE_DEFAULT = 'en';

const PAGINATION_PAGE_NAME = 'p';
const PAGINATION_ITEM_PER_PAGE = 10;
const PAGINATION_EACH_SIDE = 2;

const USER_ROLE_ADMIN = 'admin';
const USER_ROLE_USER = 'user';

const RESPONSE_STATUS_KEY = 'status';
const RESPONSE_MESSAGE_KEY = 'message';
const RESPONSE_DATA_KEY = 'data';

const RESPONSE_TYPE_SUCCESS = 'success';
const RESPONSE_TYPE_WARNING = 'warning';
const RESPONSE_TYPE_ERROR = 'error';

const ROUTE_GROUP_READER_ACCESS = 'reader_access';
const ROUTE_GROUP_CREATION_ACCESS = 'creation_access';
const ROUTE_GROUP_MODIFIER_ACCESS = 'modifier_access';
const ROUTE_GROUP_DELETION_ACCESS = 'deletion_access';
const ROUTE_GROUP_FULL_ACCESS = 'full_access';

const ROUTE_TYPE_ROLE_BASED = 'role_based_routes';
const ROUTE_TYPE_AVOIDABLE_MAINTENANCE = 'avoidable_maintenance_routes';
const ROUTE_TYPE_AVOIDABLE_UNVERIFIED = 'avoidable_unverified_routes';
const ROUTE_TYPE_AVOIDABLE_INACTIVE = 'avoidable_suspended_routes';
const ROUTE_TYPE_FINANCIAL = 'financial_routes';
const ROUTE_TYPE_GLOBAL = 'global_routes';

const ROUTE_MUST_ACCESSIBLE = 'route_must_accessible';
const ROUTE_NOT_ACCESSIBLE = 'route_not_accessible';

const ROUTE_REDIRECT_TO_UNAUTHORIZED = '401';
const ROUTE_REDIRECT_TO_UNDER_MAINTENANCE = 'under_maintenance';
const ROUTE_REDIRECT_TO_EMAIL_UNVERIFIED = 'email_unverified';
const ROUTE_REDIRECT_TO_ACCOUNT_SUSPENDED = 'account_suspended';
const ROUTE_REDIRECT_TO_FINANCIAL_ACCOUNT_SUSPENDED = 'financial_account_suspended';
const REDIRECT_ROUTE_TO_USER_AFTER_LOGIN = 'profile.index';
const REDIRECT_ROUTE_TO_LOGIN = 'login';
const ROUTE_REDIRECT_TO_DEMO_MODE = 'demo_mode';

const MENU_TYPE_ROUTE = 'route';
const MENU_TYPE_LINK = 'link';
const MENU_TYPE_PAGE = 'page';

const ACTIVE = 1;
const INACTIVE = 0;

const VERIFIED = 1;
const UNVERIFIED = 0;

const ENABLED = 1;
const DISABLED = 0;

const STATUS_ACTIVE = 'active';
const STATUS_INACTIVE = 'inactive';
const STATUS_DELETED = 'deleted';
const STATUS_OPEN = 'open';
const STATUS_IN_PROGRESS = 'progress';
const STATUS_RESOLVED = 'resolved';
const STATUS_CLOSED = 'closed';

const VISIBLE_TYPE_PUBLIC = 'public';
const VISIBLE_TYPE_PRIVATE = 'private';

const PRODUCT_VERIFIER_URL = 'https://verifier.codemen.org/api/product-verify';

//direction
const PAGE_DIRECTION_LEFT_TO_RIGHT = 'ltr';
const PAGE_DIRECTION_RIGHT_TO_LEFT = 'rtl';
