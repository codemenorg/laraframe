<?php

use Faker\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

if (!function_exists('short_code_blog_list')) {
    function short_code_blog_list(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'column' => 'required|integer',
            'item' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return '';
        }
        $validateData = $validator->validate();

        $blogs = collect([]);
        $faker = Factory::create();
        for ($x = 0; $x < $validateData['item']; $x++) {
            $blogs->push([
                'title' => $faker->sentence,
                'body' => $faker->paragraphs(3, true)
            ]);
        }

        $html = '<div class="row">';
        foreach ($blogs as $blog) {
            $html .= '<div class="col-md-' . (12 / $validateData['column']) . '">
                  <div class="mb-4 shadow-sm border">
                    <div class="post-thumbnail"><img src="'. get_image_placeholder(100, 80, 20) .'" alt="thumbnail" class="img-fluid"></div>
                    <div class="post-content p-3">
                    <h5 class="font-weight-bold"><a href="#">' . $blog['title'] . '</a></h5>
                      <p class="card-text">' . Str::limit($blog['body'], 50) . '</p>
                      <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                          <a href="#" class="btn btn-sm btn-secondary">View</a>
                          <a href="#" class="btn btn-sm btn-outline-secondary">Edit</a>
                        </div>
                        <small class="text-muted">9m</small>
                      </div>
                    </div>
                  </div>
                </div>';
        }
        $html .= '</div>';
        return $html;
    }
}
