<?php

namespace App\Models\Core;

use Carbon\Carbon;
use Database\Factories\NotificationFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'message', 'read_at'];

    protected static function newFactory(): NotificationFactory
    {
        return NotificationFactory::new();
    }

    public function scopeRead($query)
    {
        return $query->whereNotNull('read_at');
    }

    public function scopeUnread($query)
    {
        return $query->whereNull('read_at');
    }

    public function markAsRead()
    {
        return $this->update(['read_at' => Carbon::now()]);
    }

    public function markAsUnread()
    {
        return $this->update(['read_at' => null]);
    }
}
