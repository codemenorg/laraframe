<?php

namespace App\Models\Core;

use Database\Factories\TicketFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Ticket extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'user_id',
        'assigned_to',
        'previous_id',
        'title',
        'content',
        'attachment',
        'status'
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(static function ($model) {
            $model->{$model->getKeyName()} = Str::uuid()->toString();
        });
    }

    protected static function newFactory(): TicketFactory
    {
        return TicketFactory::new();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(TicketComment::class, 'ticket_id');
    }

    public function assignedUser()
    {
        return $this->belongsTo(User::class, 'assigned_to');
    }

    public function changeStatus($status): bool
    {
        if (!in_array($this->status, [STATUS_OPEN, STATUS_IN_PROGRESS])) {
            return false;
        }

        $params = [
            'status' => $status
        ];

        if (Auth::user()->assigned_role == USER_ROLE_ADMIN) {
            $params['assigned_to'] = Auth::id();
        }

        return $this->update($params);
    }
}
