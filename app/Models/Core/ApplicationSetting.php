<?php

namespace App\Models\Core;

use Exception;
use Illuminate\Database\Eloquent\Model;

class ApplicationSetting extends Model
{
    public $incrementing = false;
    protected $primaryKey = 'slug';
    protected $keyType = 'string';

    protected $fillable = [
        'slug',
        'value',
    ];

    public function getValueAttribute($value)
    {
        try {
            $fieldValue = decrypt($value);
        } catch (Exception $exception) {
            $fieldValue = $value;
        }
        return $fieldValue;
    }
}
