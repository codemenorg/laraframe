<?php

namespace App\Models\Core;

use Carbon\Carbon;
use Database\Factories\NoticeFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Notice extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'type', 'visible_type', 'start_at', 'end_at', 'is_active', 'created_by'];

    public static function boot()
    {
        parent::boot();

        static::created(function ($notice) {
            $notice = $notice->fresh();
            $notices = Cache::get('notices', collect([]));
            if ($notice->is_active) {
                $notices->push($notice);
            }
            Cache::put('notices', $notices, $notice->created_at->diffInMinutes($notice->created_at->copy()->endOfDay()));
        });

        static::updated(function ($notice) {
            $notices = Cache::get('notices', collect([]));
            $date = Carbon::now();
            if ($notice->is_active && $notice->start_at <= $date && $notice->end_at >= $date) {
                if (!$notices->find($notice->id)) {
                    $notices->push($notice);
                    Cache::put('notices', $notices, $date->diffInMinutes($date->copy()->endOfDay()));
                }
            } else {
                $notices = $notices->filter(function ($existNotice) use ($notice) {
                    return $existNotice->id != $notice->id;
                });
                Cache::put('notices', $notices, $date->diffInMinutes($date->copy()->endOfDay()));
            }
        });

    }

    protected static function newFactory(): NoticeFactory
    {
        return NoticeFactory::new();
    }

    public function scopeToday($query)
    {
        $startDate = Carbon::now();
        return $query->where(function ($q) use ($startDate) {
            $q->where('start_at', '<=', $startDate)
                ->where('end_at', '>=', $startDate);
        })->orWhere(function ($q) {
            $q->whereNull('start_at')
                ->whereNull('end_at');
        });
    }
}
