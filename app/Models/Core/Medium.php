<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Medium extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = ['name', 'path', 'mime_type', 'disk', 'file_name', 'order'];


}
