<?php

namespace App\Models\Core;

use Carbon\Carbon;
use Database\Factories\TicketCommentFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketComment extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'ticket_id', 'content', 'attachment'];

    protected static function newFactory(): TicketCommentFactory
    {
        return TicketCommentFactory::new();
    }

    public function setCreatedAt($value)
    {
        $this->attributes['created_at'] = $value ?: Carbon::now();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticket_id');
    }

}
