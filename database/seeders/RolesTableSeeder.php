<?php

namespace Database\Seeders;

use App\Models\Core\Role;
use Exception;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $adminWebPermissions = [
            'admin_section' => [
                'application-settings' => [
                    'reader_access',
                    'modifier_access',
                ],
                'user_managements' => [
                    'reader_access',
                    'creation_access',
                    'modifier_access',
                    'deletion_access',
                ],
                'notice_managements' => [
                    'reader_access',
                    'creation_access',
                    'modifier_access',
                    'deletion_access',
                ],
                'menu_manager' => [
                    'full_access',
                ],
                'log_viewer' => [
                    'reader_access',
                ],
                'language_managements' => [
                    'reader_access',
                    'creation_access',
                    'modifier_access',
                    'deletion_access',
                ],
                'page_managements' => [
                    'reader_access',
                    'creation_access',
                    'modifier_access',
                    'deletion_access',
                ],
                'media_manager' => [
                    'reader_access',
                    'creation_access',
                    'modifier_access',
                    'deletion_access',
                ],
                'tickets' => [
                    'reader_access',
                    'modifier_access',
                    'commenting_access',
                ],
            ],
        ];

        $adminApiPermissions = [
            'admin_section' => [
                'user' => [
                    'reader_access',
                ],
            ],
        ];

        $userWebPermissions = [
            'user_section' => [
                'tickets' => [
                    'reader_access',
                    'creation_access',
                    'closing_access',
                    'commenting_access',
                ],
            ],
        ];

        $userApiPermissions = [
            'user_section' => [
                'ticket' => [
                    'reader_access',
                ],
            ],
        ];

        Role::factory()->create([
            'name' => 'Admin',
            'permissions' => [
                'web' => $adminWebPermissions + $userWebPermissions,
                'api' => $adminApiPermissions + $userApiPermissions
            ],
            'accessible_routes' => [
                'web' => build_permission('web', $adminWebPermissions + $userWebPermissions, 'admin'),
                'api' => build_permission('api', $adminApiPermissions + $userApiPermissions, 'admin'),
            ],
            'is_active' => ACTIVE
        ]);


        Role::factory()->create([
            'name' => 'User',
            'permissions' => [
                'web' => $userWebPermissions,
                'api' => $userApiPermissions,
            ],
            'accessible_routes' => [
                'web' => build_permission('web', $userWebPermissions, 'user'),
                'api' => build_permission('api', $userApiPermissions, 'user'),
            ],
            'is_active' => ACTIVE
        ]);
    }
}
