<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(NotificationsTableSeeder::class);
        $this->call(NoticesTableSeeder::class);
        $this->call(TicketsTableSeeder::class);
    }
}
