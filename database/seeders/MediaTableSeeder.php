<?php

namespace Database\Seeders;

use App\Models\Core\Medium;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class MediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $params = [
            [
                'id' => Str::uuid()->toString(),
                'name' => 'big arrow plane',
                'path' => 'media',
                'file_name' => 'big-arrow-plane.png',
                'mime_type' => 'image/png',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'big imac',
                'path' => 'media',
                'file_name' => 'big-imac.png',
                'mime_type' => 'image/png',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'big rainbow',
                'path' => 'media',
                'file_name' => 'big-rainbow.png',
                'mime_type' => 'image/png',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'big yellow arrow',
                'path' => 'media',
                'file_name' => 'big-yellow-arrow.png',
                'mime_type' => 'image/png',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'Rana',
                'path' => 'media',
                'file_name' => 'rana.jpeg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'Zahid',
                'path' => 'media',
                'file_name' => 'zahid.jpeg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'Rabbi',
                'path' => 'media',
                'file_name' => 'rabbit.jpeg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'Nazmul',
                'path' => 'media',
                'file_name' => 'nazmul.jpeg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'about us',
                'path' => 'media',
                'file_name' => 'about-us.png',
                'mime_type' => 'image/png',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'service one',
                'path' => 'media',
                'file_name' => 'service1.jpg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'service two',
                'path' => 'media',
                'file_name' => 'service2.jpg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'service three',
                'path' => 'media',
                'file_name' => 'service3.jpg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'working bg',
                'path' => 'media',
                'file_name' => 'working-bg.jpg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'gallery one',
                'path' => 'media',
                'file_name' => 'gallery1.jpg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'gallery two',
                'path' => 'media',
                'file_name' => 'gallery2.jpg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'gallery three',
                'path' => 'media',
                'file_name' => 'gallery3.jpg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'gallery four',
                'path' => 'media',
                'file_name' => 'gallery4.jpg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'gallery five',
                'path' => 'media',
                'file_name' => 'gallery5.jpg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid()->toString(),
                'name' => 'gallery six',
                'path' => 'media',
                'file_name' => 'gallery6.jpg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ];

        Medium::insert($params);
    }
}
