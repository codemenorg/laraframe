<?php

namespace Database\Factories;

use App\Models\Core\Notice;
use Illuminate\Database\Eloquent\Factories\Factory;

class NoticeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Notice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->sentence,
            'description' => $this->faker->paragraph,
            'type' => $this->faker->randomElement(array_keys(notices_types())),
            'visible_type' => $this->faker->randomElement(array_keys(notices_visible_types())),
            'start_at' => $this->faker->dateTimeThisMonth('now'),
            'end_at' => $this->faker->dateTimeBetween('+1 days', '+1 months'),
            'is_active' => $this->faker->boolean,
            'created_by' => 1
        ];
    }
}
