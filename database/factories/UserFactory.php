<?php

namespace Database\Factories;

use App\Models\Core\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'assigned_role' => USER_ROLE_USER,
            'username' => $this->faker->userName,
            'email' => $this->faker->unique()->email,
            'password' => Hash::make('user'),
            'is_accessible_under_maintenance' => $this->faker->boolean,
            'is_email_verified' => $this->faker->boolean,
            'is_super_admin' => false,
            'status' => $this->faker->randomElement([STATUS_INACTIVE, STATUS_ACTIVE, STATUS_DELETED]),
        ];
    }
}
